# README #

### What is this repository for? ###

* This project is split into 2 submodules
* 	- server - used for running Node.JS app with MongoDB to provide REST API for client
*	- client - Angular application used for listing, creating and managind sport records

### How do I get set up? ###

* MongoDB instance must be running, usually on port 27017 under admin/admin account
* Both client and server are handled via NPM

* cd server; npm install; npm start
* cd client; npm install; npm run dev