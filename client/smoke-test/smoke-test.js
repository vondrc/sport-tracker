const phantom = require('phantom');

(async function() {
  const instance = await phantom.create();
  const page = await instance.createPage();
  await page.property('viewportSize', { width: 1024, height: 768 });
  await page.property('clipRect', { top: 0, left: 0, width: 1024, height: 768 });
  await page.on("onResourceRequested", function(requestData) {
    console.info('Requesting', requestData.url)
  });

  const login = await page.open("http://localhost:4000/login");
  console.log(login);

  await page.render('smoke-test/jpg/login.jpg');

  await page.evaluate(function() {
    document.getElementById("login-btn").click();
  });

  await page.render('smoke-test/jpg/login-clicked.jpg');

  const recordAdd = await page.open("http://localhost:4000/records/add");
  console.log(recordAdd);
  await page.render('smoke-test/jpg/add-form.jpg');

  const records = await page.open("http://localhost:4000/records");
  console.log(records);
  await page.render('smoke-test/jpg/records.jpg');


  await instance.exit();
}());
