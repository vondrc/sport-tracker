import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { HttpModule, JsonpModule } from "@angular/http";
import { BsDropdownModule } from 'ngx-bootstrap';

import { RecordModule } from "../record/record.module";
import { RecordsRoutesModule } from "../../routes/router.module";
import { ChartsModule } from "../charts/charts.module";
import { UserModule } from "../user/user.module";

import { AppComponent } from "./app.component";

import { RecordService } from "../record/services/record.service";
import HttpService from "../../services/http.service";


const appRoutes: Routes = [];

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    RecordModule,
    RecordsRoutesModule,
    RouterModule.forRoot(appRoutes),
    ChartsModule,
    BsDropdownModule.forRoot(),
    UserModule
  ],
  exports: [],
  declarations: [
    AppComponent
  ],
  providers: [RecordService, HttpService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
