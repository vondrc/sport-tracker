import { Component } from "@angular/core";

import { ChartService } from "./services/chart.service";

@Component({
  selector: 'charts-comp',
  templateUrl: './charts.component.html'
})
export class ChartComponent {

  constructor(private chartService: ChartService) {
  }

  private chart1;
  private chart2;
  private options1 = {
    chart: { type: 'pie' },
    title: { text: 'Sport category ratio' },
    series: [{}]
  };
  private options2 = {
    chart: {
      type: 'bar'
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Total distance per category (kilometers)',
        align: 'high'
      },
      labels: {
        overflow: 'justify'
      }
    },
    tooltip: {
      valueSuffix: ' kilometers'
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true
        }
      }
    },
    title: {
      text: 'Bar chart'
    },
    series: [{}]
  }

  ngOnInit() {
  }

  saveChart1(chart) {
    this.chart1 = chart;
    this.chartService.getChartsData().subscribe(
      response => {
        this.chart1.series[0].setData(response);
      }
    )
  }

  saveChart2(chart) {
    this.chart2 = chart;
    this.chartService.countCatDistance().subscribe(
      response => {
        this.chart2.xAxis[0].setCategories(response[1]);
        this.chart2.series[0].setData(response[0]);
      }
    )
  }
}

