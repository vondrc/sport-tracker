import { NgModule } from "@angular/core";
import { ChartModule } from 'angular2-highcharts';

import { ChartComponent } from "./charts.component";
import { ChartService } from "./services/chart.service";

@NgModule({
  imports: [ChartModule.forRoot(require('highcharts'))],
  declarations: [ChartComponent],
  providers: [ChartService],
  exports: [ChartComponent]
})
export class ChartsModule {
}
