import { Injectable } from '@angular/core';
import HttpService from "../../../services/http.service";

@Injectable()
export class ChartService {

  constructor(private httpService: HttpService) {
  }

  private recordsUrl = 'api/record';

  getChartsData() {
    return this.httpService.get(this.recordsUrl)
      .map(data => {
        let types = [], a = [], b: any = [], c = [], prev;
        data.map(res => {
          types.push(res._type[0]);
        });
        types.sort();
        for (let i = 0; i < types.length; i++) {
          if (types[i] !== prev) {
            a.push({ "name": types[i] });
            b.push({ "y": 1 });
          } else {
            b[b.length - 1].y++;
          }
          prev = types[i];
        }

        for (let i = 0; i < a.length; i++) {
          c.push(Object.assign(a[i], b[i]));
        }
        return c;
      });
  }

  countCatDistance() {
    return this.httpService.get(this.recordsUrl)
      .map(res => {
        let targetObj = {}, chartData = [], xAxies = [];
        for (let i = 0; i < res.length; i++) {
          if (!targetObj.hasOwnProperty(res[i]._type[0])) {
            targetObj[res[i]._type] = 0;
          }
          targetObj[res[i]._type] += res[i]._distance;
        }
        Object.entries(targetObj).forEach(
          ([key, value]) => {
            chartData.push({ name: key, y: value });
            xAxies.push(key);
          }
        );
        return [chartData, xAxies];
      });
  }
}
