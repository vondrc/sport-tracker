import { Component } from "@angular/core";

import { UserService } from "../user/services/user.service";

@Component({
  selector: 'logout',
  templateUrl: './logout.component.html',
})
export class UserLogoutComponent {

  constructor(private userService: UserService) {
  }

  logoutUser() {
    event.preventDefault();
    this.userService.logout();
  }
}


