import { AfterViewInit, Component, ElementRef } from "@angular/core";
import { Observable } from "rxjs";
import 'rxjs/add/observable/of';

import { RecordService } from "../services/record.service";
import { Record } from "../../../models/record.model";

@Component({
  selector: 'add-record',
  templateUrl: './record-add.component.html'
})
export class RecordAddComponent implements AfterViewInit {
  private resMessage: string;
  private errMessage: string;
  private recordData: Record;
  private recordTypes = ['', 'Running', 'Swimming', 'Hiking', 'Cycling', 'Windsurfing', 'Yachting', 'Hiking', 'Eating'];
  private myTypeaheads: Record;
  private showList;

  constructor(private recordService: RecordService, private elementRef: ElementRef) {
    this.recordData = new Record();
  }

  ngAfterViewInit() {
    let nameInput = this.elementRef.nativeElement.querySelector('#Name');

    Observable.fromEvent(nameInput, 'keyup')
      .debounceTime(600)
      .distinctUntilChanged()
      .filter(x => x !== '')
      .switchMap(() => this.recordService.getRecordsByName(nameInput.value))
      .subscribe(res => this.myTypeaheads = res);
  }

  submitRecord() {
    this.recordService.addRecord(this.recordData).subscribe(
      response => {
        this.resMessage = "Record created!";
      },
      error => {
        this.errMessage = "Ups, can\'t create record!";
        console.log(error);
      });
  }

  fillForm(selected) {
    this.recordData.name = selected._name;
    this.recordData.type = selected._type;
    this.recordData.duration = selected._duration;
  }
}
