import { Component, OnInit } from "@angular/core";
import { Record } from "../../models/record.model"
import { RecordService } from "./services/record.service";

@Component({
  selector: 'record-comp',
  templateUrl: './record.component.html'
})

export class RecordComponent implements OnInit {
  private recordsData: Record[];
  private errorMessage: any;


  constructor(private recordService: RecordService) {
  }

  ngOnInit() {
    this.getRecords();
  }

  getRecords() {
    this.recordService.getRecords().subscribe(
      response => this.recordsData = <any>response,
      error => this.errorMessage = <any>error);
  }
}
