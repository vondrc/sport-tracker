import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { HttpModule, JsonpModule } from "@angular/http";

import { RecordComponent } from "./record.component";
import { RecordAddComponent } from "./add-record/record-add.component";
import { PipeModule } from "../../pipes/pipe.module";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    PipeModule.forRoot(),
  ],
  declarations: [
    RecordComponent,
    RecordAddComponent
  ],
  providers: [],
  exports: [RecordComponent]
})
export class RecordModule {
}
