import { Injectable } from '@angular/core';
import HttpService from "../../../services/http.service";

@Injectable()
export class RecordService {

  constructor(private httpService: HttpService) {
  }

  private recordsUrl = 'api/record';
  private addRecordUrl = 'api/record/create';
  private recordsByNameUrl = 'api/record/name?name=';

  getRecords() {
    return this.httpService.get(this.recordsUrl);
  }

  addRecord(data) {
    return this.httpService.post(this.addRecordUrl, data);
  }

  getRecordsByName(query: string) {
    return this.httpService.get(this.recordsByNameUrl + query);
  }
}
