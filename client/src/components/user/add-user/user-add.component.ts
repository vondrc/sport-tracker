import { Component, ElementRef } from "@angular/core";
import { UserService } from "../services/user.service";
import { User } from "../../../models/user.model";

const jwt = require('jwt-simple');

@Component({
  selector: 'add-user',
  templateUrl: './user-add.component.html'
})

export class UserAddComponent {
  private resMessage: string;
  private errMessage: string;
  private userData: User;
  private userCountries = this.userService.countries;
  private gender = this.userService.gender;

  constructor(private userService: UserService, private elementRef: ElementRef) {
    this.userData = new User();
  }

  submitUser() {
    const payload = this.userData;
    const secret = 'xxx';
    const encodeUser = jwt.encode(payload, secret);
    const user = { "user": encodeUser };
    this.userService.addUser(user).subscribe(
      response => {
        this.resMessage = "User created!";
      },
      error => {
        this.errMessage = "Ups, can\'t create user!";
        console.log(error);
      });
  }
}
