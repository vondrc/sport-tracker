import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

const jwt = require('jwt-simple');

import HttpService from "../../../services/http.service";
import { User } from "../../../models/user.model";

@Injectable()
export class UserService {

  constructor(private httpService: HttpService, private route: ActivatedRoute, private router: Router) {
  }

  private userUrl = 'api/user/';
  private userAuthUrl = 'api/user/auth';
  private addUserUrl = 'api/user/create';
  public user: User;
  public countries = ["Cuba", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia"];
  public gender = ["Male", "Female"];

  getLoggedUser() {
    let token = localStorage.getItem("userToken");
    let decoded = jwt.decode(token, "xxx", true);
    return this.httpService.get(this.userUrl + decoded.id);
  }

  loginUser(username, password) {
    let data = { "_name": username, "_password": password };
    return this.httpService.post(this.userAuthUrl, data).subscribe(
      data => {
        this.user = data;
        localStorage.setItem('userToken', JSON.stringify(data._token));
        this.router.navigate(["/user"]);
      },
      error => {
        alert("Login failed!");
        console.log("login failed");
        return false;
      });
  }

  logout() {
    localStorage.removeItem('userToken');
    this.router.navigate(["/login"]);
  }

  addUser(data) {
    return this.httpService.post(this.addUserUrl, data);
  }

  updateUser(data) {
    return this.httpService.put(this.userUrl + data._id, data);
  }

}
