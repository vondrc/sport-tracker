import { Component } from "@angular/core";

import { UserService } from "../services/user.service";
import { User } from "../../../models/user.model";

@Component({
  selector: 'user-login',
  templateUrl: './user-login.component.html'
})
export class UserLoginComponent {
  private model: any = {};
  private user: User[];

  constructor(private userService: UserService) {
  }

  login() {
    this.userService.loginUser(this.model.username, this.model.password);
  }
}
