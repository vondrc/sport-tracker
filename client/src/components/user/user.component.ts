import { Component, OnInit } from "@angular/core";
import { UserService } from "./services/user.service";

@Component({
  selector: 'user-comp',
  templateUrl: './user.component.html'
})
export class UserComponent implements OnInit {
  private userData: any;
  private errorMessage: any;
  private successMessage: string;
  private userCountries = this.userService.countries;
  private gender = this.userService.gender;
  private showEditForm: boolean = false;

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    if (this.userService.user) {
      this.userData = this.userService.user;
    } else {
      this.userData = '';
      this.getUser();
    }
  }

  getUser() {
    this.userService.getLoggedUser().subscribe(
      data => this.userData = <any>data,
      error => this.errorMessage = <any>error);
  }

  updateUser() {
    this.userService.updateUser(this.userData).subscribe(
      data => {
        this.userData = <any>data;
        this.successMessage = "User updated"
      },
      error => this.errorMessage = <any>error);
  }
}
