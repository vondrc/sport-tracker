import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { HttpModule, JsonpModule } from "@angular/http";
import { RouterModule } from "@angular/router";

import { PipeModule } from "../../pipes/pipe.module";

import { UserService } from "./services/user.service";

import { UserComponent } from "./user.component";
import { UserAddComponent } from "./add-user/user-add.component";
import { UserLoginComponent } from "./user-login/user-login.component";
import { UserLogoutComponent } from "../logout/logout.component";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    PipeModule.forRoot(),
    RouterModule
  ],
  declarations: [
    UserComponent,
    UserAddComponent,
    UserLoginComponent,
    UserLogoutComponent
  ],
  providers: [UserService],
  exports: [UserComponent, UserLogoutComponent]
})
export class UserModule {
}
