export class Record {
  private _id: string;
  private _type: any;
  private _name: string;
  private _description: string;
  private _distance: number;
  private _created: any;
  private _updated: any;
  private _duration: number;

  get duration(): number {
    return this._duration;
  }

  set duration(value: number) {
    this._duration = value;
  }

  get created(): any {
    return this._created;
  }

  set created(value: any) {
    this._created = value;
  }

  get distance(): number {
    return this._distance;
  }

  set distance(value: number) {
    this._distance = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get type(): any {
    return this._type;
  }

  set type(value: any) {
    this._type = value;
  }
}
