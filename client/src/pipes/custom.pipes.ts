import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'myDurationPipe' })
export class DurationPipe implements PipeTransform {
  transform(value: number): any {
    const hours = Math.floor(value / 3600);
    const minutes = Math.floor((value - (hours * 3600)) / 60);
    const seconds = value - (hours * 3600) - (minutes * 60);

    // round seconds
    let sec = Math.round(seconds * 100) / 100;

    let result = (hours < 10 ? "0" + hours : hours);
    result += ":" + (minutes < 10 ? "0" + minutes : minutes);
    result += ":" + (sec < 10 ? "0" + sec : sec);
    return result;
  }
}
