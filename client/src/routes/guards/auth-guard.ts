import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
const jwt = require('jwt-simple');

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.checkToken();
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }

  checkToken(): boolean {
    let token = JSON.parse(localStorage.getItem("userToken"));
    if (token) {
      let decoded = jwt.decode(token, "xxx", true);
      if (decoded && decoded.valid > Date.now()) {
        return true;
      } else {
        alert("Session expired, please log in.");
        localStorage.removeItem("userToken");
        this.router.navigate(["/login"]);
        return false;
      }
    } else {
      alert("Session expired, please log in.");
      this.router.navigate(["/login"]);
      return false;
    }
  }
}
