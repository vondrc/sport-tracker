import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RecordComponent } from "../components/record/record.component";
import { RecordAddComponent } from '../components/record/add-record/record-add.component';
import { ChartComponent } from "../components/charts/charts.component";
import { UserComponent } from "../components/user/user.component";
import { UserAddComponent } from "../components/user/add-user/user-add.component";
import { UserLoginComponent } from "../components/user/user-login/user-login.component";

import { AuthGuard } from "./guards/auth-guard";

const recordsRoutes: Routes = [
  {
    path: 'records',
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: RecordComponent
      },
      {
        path: 'add',
        component: RecordAddComponent
      }
    ]
  },
  { path: 'charts', component: ChartComponent, canActivate: [AuthGuard] },
  { path: 'user', component: UserComponent, canActivate: [AuthGuard] },
  { path: 'login', component: UserLoginComponent },
  { path: 'newUser', component: UserAddComponent },
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [
    RouterModule.forChild(recordsRoutes)
  ],
  declarations: [],
  providers: [AuthGuard],
  exports: [RouterModule]
})
export class RecordsRoutesModule {

}
