const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const app = express();
const api = express.Router();
const apiUser = express.Router();
const apiRecords = express.Router();

require('./routes/login')(api);
require('./routes/records')(apiRecords);
require('./routes/users')(apiUser);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api/user', apiUser);
app.use('/api/record', apiRecords);
app.use('/api', api);

mongoose.connect('mongodb://localhost/sport');
mongoose.connection.on('error', (error) => console.error('Unable to connect to MongoDB instance.', error));
mongoose.connection.on('open', () => {
  console.log('MongoDB connected');
  app.listen(3000, () => console.log('Server started on port 3000'));
});
