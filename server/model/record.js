const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const recordSchema = new Schema({
  _type: {
    type: Array
  },
  _name: {
    type: String
  },
  _description: {
    type: String
  },
  _distance: {
    type: Number
  },
  _created: {
    type: Date
  },
  _updated: {
    type: Date
  },
  _duration: {
    type: Number
  }
});

const record = mongoose.model('record', recordSchema);
module.exports = record;
