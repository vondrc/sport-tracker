const mongoose =  require("mongoose");

const Schema = mongoose.Schema;

const userSchema = new Schema({
  _name: {
    type: String
  },
  _email: {
    type: String
  },
  _age: {
    type: Number
  },
  _created: {
    type: Date, default: Date.now
  },
  _updated: {
    type: Date, default: Date.now
  },
  _country: {
    type: String
  },
  _sex: {
    type: String
  },
  _password: {
    type: String
  }

});

const user = mongoose.model('user', userSchema);
module.exports = user;
