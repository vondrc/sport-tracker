const recordModel = require("../model/record");

class RecordRepository {
  constructor() {
  }

  //get all records
  getRecord(callback) {
    const instance = recordModel;
    return instance.find(callback);
  }

  // get one by id
  getRecordById(id, callback) {
    const instance = recordModel;
    return instance.findById(id, callback);
  }

  getRecordByName(queryParams, callback) {
    const instance = recordModel;
    return instance.find({ "_name": { $regex: [queryParams], $options: '<i>' } }, callback);
  }

  //update record
  updateRecord(id, body, options, callback) {
    var query = { _id: id };
    const instance = recordModel;
    return instance.findOneAndUpdate(query, { $set: body }, options, callback);
  }

  //Add Record
  create(record, callback) {
    const instance = recordModel;
    return instance.create(record, callback);
  }

}

module.exports = new RecordRepository();
