const userModel = require("../model/user");

class UserRepository {
  constructor() {
  }

  //get all users
  getUser(callback) {
    const instance = userModel;
    return instance.find(callback);
  }

  //add user
  createUser(record, callback) {
    const instance = userModel;
    return instance.create(record, callback);
  }

  // // get one by id
  getUserById(id, callback) {
    const instance = userModel;
    return instance.findById(id, callback);
  }

  authUser(username, callback) {
    const instance = userModel;
    return instance.findOne({ '_name': username }, callback);
  }

  //update user
  updateUser(id, body, options, callback) {
    var query = { _id: id };
    const instance = userModel;
    return instance.findOneAndUpdate(query, { $set: body }, options, callback);
  }
}

module.exports = new UserRepository();
