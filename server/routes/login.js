const jwt = require("jwt-simple");

module.exports = function(api) {

  api.use((req, res, next) => {
    console.log('Time: ', new Date());
    next();
  });

  api.get('/login', (req, res) => {
    console.log('login request');
    const payload = { user: 'vondrc', valid: Date.now() + (10 * 60 * 1000) };
    const secret = 'xxx';
    const token = jwt.encode(payload, secret);
    res.json(token);
  });

};
