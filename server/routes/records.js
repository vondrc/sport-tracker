const RecordService = require('../service/record.service');

module.exports = function(api) {

  api.use((req, res, next) => {
    console.log('Time: ', new Date());
    next();
  });

  api.get('/', (req, res) => {
    RecordService.getAllRecords(req, function(err, record) {
      if (err) {
        console.error(err.stack);
        return res.status(404).send('Something is wrong!');
      }
      res.json(record);
    });
  });

  api.get('/name', (req, res) => {
    RecordService.getRecordByName(req, req.query.name, function(err, record) {
      if (err) {
        console.error(err.stack);
        return res.status(404).send('Something is wrong!');
      }
      res.json(record);
    });
  });

  api.get('/:id', (req, res) => {
    RecordService.getRecordById(req, req.params.id, function(err, record) {
      if (err) {
        console.error(err.stack);
        return res.status(404).send('Something is wrong!');
      }
      res.json(record);
    });
  });

  api.put('/:id', (req, res) => {
    RecordService.updateRecord(req, req.params.id, req.body, { upsert: true }, function(err, record) {
      if (err) {
        console.error(err.stack);
        return res.status(404).send('Something is wrong!');
      }
      res.json(record);
    });
  });

  api.post('/create', function(req, res) {
    RecordService.createRecord(req, req.body, function(err, genre) {
      if (err) {
        console.error(err.stack);
        return res.status(404).send('Something is wrong!');
      }
      res.json(genre);
    });
  });

};
