const jwt = require("jwt-simple");

const UserService = require('../service/user.service');

module.exports = function(api) {

  api.use((req, res, next) => {
    console.log('Time: ', new Date());
    next();
  });

  api.put('/:_id', (req, res) => {
    UserService.updateUser(req, req.params._id, req.body, { upsert: true, new: true }, function(err, record) {
      if (err) {
        console.error(err.stack);
        return res.status(404).send('Something is wrong!');
      }
      res.json(record);
    });
  });

  api.get('/', (req, res) => {
    UserService.getAllUsers(req, function(err, record) {
      if (err) {
        console.error(err.stack);
        return res.status(404).send('Something is wrong!');
      }
      res.json(record);
    });
  });

  api.post('/create', function(req, res) {
    UserService.createUser(req, req.body, function(err, genre) {
      if (err) {
        console.error(err.stack);
        return res.status(404).send('Something is wrong!');
      }
      res.json(genre);
    });
  });

  api.get('/:_id', (req, res) => {
    UserService.getUserById(req, req.params._id, function(err, record) {
      if (err) {
        console.error(err.stack);
        return res.status(404).send('Something is wrong!');
      }
      res.json(record);
    });
  });

  api.post('/auth', (req, res) => {
    UserService.authenticate(req, req.body._name, function(err, user) {
      if (err) {
        console.error(err.stack);
        return res.status(404).send('Something is wrong!');
      }
      if (user && req.body._password == user._password) {
        // authentication successful
        const payload = { id: user._id, valid: Date.now() + (10 * 60 * 1000) };
        const secret = 'xxx';
        const token = jwt.encode(payload, secret);
        res.json(
          {
            _id: user._id,
            _name: user._name,
            _email: user._email,
            _age: user._age,
            _country: user._country,
            _sex: user._sex,
            _token: token
          }
        );
      } else {
        // authentication failed
        console.log("auth failed");
        return res.status(500).send("authentication failed");
      }
    });
  });
};
