const repository = require('../repository/record.repository');

class RecordService {
  constructor() {
  }

  getAllRecords(req, callback) {
    console.log('"Get all"', req.method, req.originalUrl);
    repository.getRecord(callback);
  }

  getRecordById(req, id, callback) {
    console.log('"Get one"', req.method, req.originalUrl);
    repository.getRecordById(id, callback);
  }

  getRecordByName(req, queryParams, callback) {
    console.log('"Get by name"', req.method, req.originalUrl);
    repository.getRecordByName(queryParams, callback);
  }

  updateRecord(req, id, body, options, callback) {
    body.updated = Date.now();
    console.log('"Update one"', req.method, req.originalUrl);
    console.log(body);
    repository.updateRecord(id, body, options, callback);
  }

  createRecord(req, record, callback) {
    console.log('"Create new"', req.method, req.originalUrl);
    repository.create(record, callback);
  }
}

module.exports = new RecordService;
