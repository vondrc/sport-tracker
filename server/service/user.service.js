const jwt = require("jwt-simple");

const repository = require('../repository/user.repository');

class UserService {
  constructor() {
  }

  getAllUsers(req, callback) {
    console.log('"Get all"', req.method, req.originalUrl);
    repository.getUser(callback);
  }

  createUser(req, userObj, callback) {
    let decodedUser = jwt.decode(userObj.user, "xxx", true);
    console.log('"Create new"', req.method, req.originalUrl);
    repository.createUser(decodedUser, callback);
  }

  getUserById(req, id, callback) {
    console.log('"Get one"', req.method, req.originalUrl);
    repository.getUserById(id, callback);
  }

  authenticate(req, username, callback) {
    console.log('"Login"', req.method, req.originalUrl);
    repository.authUser(username, callback);
  }

  updateUser(req, id, body, options, callback) {
    body.updated = Date.now();
    console.log('"Update one"', req.method, req.originalUrl);
    repository.updateUser(id, body, options, callback);
  }
}

module.exports = new UserService();
